extractExtTemperature <- function(file, timeS, timeE) {
  
  extTemperature <- read.csv(file, sep = ";", dec = ".")
  
  if(timeS == "") {
    dateS <- as.POSIXct("1111-01-01", tz = "GMT+0100")
  } else {
    dateS <- as.POSIXct(timeS, tz = "GMT+0100")
  }
  
  if(timeE == "") {
    dateE <- as.POSIXct("9999-01-01", tz = "GMT+0100")
  } else {
    dateE <- as.POSIXct(timeE, tz = "GMT+0100")
  }
  
  extTemperature$ext <- as.numeric(as.character(extTemperature$ext))
  extTemperature$real <- as.POSIXct(extTemperature$date, tz = "GMT+0100") 

  resultTemperature <- data.frame(date = extTemperature$real, temperature =  extTemperature$ext)
  resultTemperature <- resultTemperature %>% filter(date >= dateS) %>% filter(date <= dateE) %>% na.omit()
  
  return (resultTemperature)
}


preTraitementTemperature <- function(root_path, path_temperature, mandrill_uuid, timeS, timeE, all = FALSE) {
  print("Start parse temperature")
  
  if(all) {
    pathD <- paste0(root_path,"/temperature_all.csv")
  } else {
    pathD <- paste0(root_path,"/temperature.csv")
  }
  
  
  unlink(pathD, recursive=TRUE)
  
  file.create(pathD)
  
  if(timeS == "") {
    timeS <- "2000-01-01"
  } 
  
  if(timeE == "") {
    timeE <- "2030-01-01"
  }

  CPPparseTemperature(path_temperature, pathD, mandrill_uuid, timeS, timeE)
  
  gc(reset = TRUE)

  print("End parse temperature")
}

preTraitementAcceleration <- function(root_path, path_acceleration, mandrill_uuid, timeS, timeE) {
  print("Start parse acceleration")
  
  pathD <- paste0(root_path,"/acceleration.csv")
  
  unlink(pathD, recursive=TRUE)
  
  file.create(pathD)
  
  if(timeS == "") {
    timeS <- "2000-01-01"
  } 
  
  if(timeE == "") {
    timeE <- "2030-01-01"
  }
  
  CPPparseAcceleration(path_acceleration, pathD, mandrill_uuid, timeS, timeE)
  
  print("End parse acceleration")
}


preTraitementMouvement <- function(root_path, path_mouvement, mandrill_uuid, timeS, timeE, all = FALSE) {
  print("Start parse mouvement")
  
  if(all) {
    pathD <- paste0(root_path,"/mouvement_all.csv")
  } else {
    pathD <- paste0(root_path,"/mouvement.csv")
  }
  
  unlink(pathD, recursive=TRUE)
  
  file.create(pathD)
  
  if(timeS == "") {
    timeS <- "2000-01-01"
  } 
  
  if(timeE == "") {
    timeE <- "2030-01-01"
  }
  
  CPPparseMouvement(path_mouvement, pathD, mandrill_uuid, timeS, timeE)
  
  print("End parse mouvement")
}

readCSVTemperature <- function(path) {
  resultTemperature <- read.csv(path, sep = ";")
  resultTemperature$date <- as.POSIXct(as.character(resultTemperature$date), tz = "GMT+0100")
  return(resultTemperature)
}

readCSVMouvement <- function(path) {
  resultMouvement <- read.csv(path, sep = ";")
  resultMouvement <- resultMouvement %>% na.omit()
  resultMouvement$date <- as.POSIXct(as.character(resultMouvement$date), tz = "GMT+0100")
  return(resultMouvement)
}

readCSVAcceleration <- function(path) {
  resultAcceleration <- read.csv(path, sep = ";")
  resultAcceleration <- resultAcceleration %>% na.omit()
  resultAcceleration$date <- as.POSIXct(as.character(resultAcceleration$date), tz = "GMT+0100")
  return(resultAcceleration)
}
