#include <Rcpp.h> 
#include <vector>
#include <fstream>
#include <map>
#include <time.h>


using namespace std;
using namespace Rcpp;

std::map<std::string, std::string> months { {"Jan", "01"} , {"Feb", "02"}, {"Mar", "03"}, {"Apr", "04"}, {"May", "05"}, {"Jun", "06"},  {"Jul", "07"}, {"Aug", "08"}, {"Sep", "09"}, {"Oct", "10"}, {"Nov", "11"}, {"Dec", "12"} }; 

class MandrillT {

public:
  int iteration = 0;
  double temperature = 0.0;
  
  MandrillT() = default;
  
  ~MandrillT() = default;
  
  double getComputeT() {
    return temperature / (double)iteration;
  }
  
  void addTemperature(double tmp) {
    iteration++;
    temperature += tmp;
  }
  
  void rezet() {
    iteration = 0;
    temperature = 0.0;
  }
};

class MandrillM {
  
public:
  int iteration = 0;
  long long mouvement = 0;
  
  MandrillM() = default;
  
  ~MandrillM() = default;
  
  void addMouvement(int mouv) {
    if(mouv > 0) {
      iteration++;
    }
    mouvement += mouv;
  }
  
  void rezet() {
    iteration = 0;
    mouvement = 0.0;
  }
};

//[[Rcpp::plugins(cpp11)]]

vector<string> split (string s, string delimiter) {
  size_t pos_start = 0, pos_end, delim_len = delimiter.length();
  string token;
  vector<string> res;
  
  while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
    token = s.substr (pos_start, pos_end - pos_start);
    pos_start = pos_end + delim_len;
    res.push_back (token);
  }
  
  res.push_back (s.substr (pos_start));
  return res;
}

std::string dec2hex(int i) {
  stringstream ss;
  ss << hex << uppercase << i;
  return ss.str();
}

std::string bitwNot(string value) {
  
  std::string result = "";
  
  for (int i=0; i<value.size(); i++) {
    if(value[i] == '0') {
      result += "1";
    }
    else {
      result += "0";
    }
  }
  
  return result;
  
}

bool is_number(const std::string &s) {
  return !s.empty() && std::all_of(s.begin(), s.end(), ::isdigit);
}

std::string decToBinary(int n) { 
  if (n<0){ // check if negative and alter the number
    n = 256 + n;
  }
  std::string result = "";
  while(n > 0){
    result = std::string(1, (char) (n%2 + 48)) + result;
    n = n/2;
  }
  return result;
} 

time_t dateToint(std::string v) {
  struct tm tm;
  time_t t;
  strptime(v.c_str(), "%Y-%m-%d %H:%M:%S", &tm);
  tm.tm_isdst = -1;     
  t = mktime(&tm);
  return t;
}

time_t dateToint2(std::string v) {
  struct tm tm;
  time_t t;
  strptime(v.c_str(), "%d/%m/%Y %H:%M:%S", &tm);
  tm.tm_isdst = -1;     
  t = mktime(&tm);
  return t;
}

std::string intTodate(time_t t) {
  struct tm * timeinfo; 
  timeinfo = localtime ( &t );
  
  std::string year = std::to_string(timeinfo->tm_year+1900);
  
  std::string month = std::to_string(timeinfo->tm_mon+1);
  month = (month.size()==1) ? "0"+month : month;
  
  std::string day = std::to_string(timeinfo->tm_mday);
  day = (day.size()==1) ? "0"+day : day;
  
  std::string hour = std::to_string(timeinfo->tm_hour);
  hour = (hour.size()==1) ? "0"+hour : hour;
  
  std::string min = std::to_string(timeinfo->tm_min);
  min = (min.size()==1) ? "0"+min : min;
  
  std::string sec = std::to_string(timeinfo->tm_sec);
  sec = (sec.size()==1) ? "0"+sec : sec;
  
  std::string result = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
  return result;
}

// [[Rcpp::export]]
void CPPparseExternTemperature(StringVector paths, std::string output_path) {
  
  std::vector<std::string> ro;
  
  for(int i=0; i < paths.size(); i++){
    //std::cout << paths[i] << '\n';
    
    int index = 0;
    
    std::ifstream input( paths[i] );
    
    for( std::string line; getline( input, line ); )
    {
      
      if(index >= 2) {
        
        vector<string> vline = split(line, "\t");
        
        std::string date1 = vline[0];
        std::string date2 = vline[1];
        
        std::string date = "20"+intTodate(dateToint2(date1 + " " + date2 + ":00"));
        
        auto tempa = date + ";" + vline[2] + "\n";
        ro.push_back(tempa);
      }
      
      index++;
    }
    
    input.close();
  }
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"ext\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  }
  
  output.close();
}

// [[Rcpp::export]]
void CPPcomputeExternTemperature(std::string data_path, std::string output_path, std::string date_start, int compute_time) {
  
  std::cout << "Start compute ext temperature" << '\n';
  
  time_t timeDateStart = dateToint(date_start);
  
  std::vector<std::string> ro;
  
  std::ifstream input( data_path );
  
  bool first = true;
  bool second = true;
  
  double lastTemperature = 0.0;
  
  
  for( std::string line; getline( input, line ); )
  {
    
    if(first) {
      first = false;
    } else {
      
      vector<string> vline = split(line, ";");
      
      std::string date = vline[0];
      double temperature = std::stod(vline[1]);
      
      time_t intD = dateToint(date);
      

      if(timeDateStart >= intD ) {
        
        //std::cout << intTodate(timeDateStart) << '\n';
        
        std::string dateCompute = intTodate(timeDateStart);
        
        std::string stemp = to_string(temperature);
        
        std::string result = dateCompute + ";" + stemp.substr(0, stemp.find(".") + 2) + "\n";
        //output<<result<< '\n';
        ro.push_back(result);
      
        timeDateStart += compute_time;
        
        lastTemperature = temperature;
      } else {
        
        while(timeDateStart < intD) {
          
          std::string dateCompute = intTodate(timeDateStart);
          std::string result;
          
          if(timeDateStart + 60*60*1 < intD) {
            result = dateCompute + ";" + "0.0" + "\n";
          } else {
            std::string stemp = to_string(lastTemperature);
            result = dateCompute + ";" + stemp.substr(0, stemp.find(".") + 2) + "\n";
          }
          
          //output<<result<< '\n';
          ro.push_back(result);
          
          timeDateStart += compute_time;
        }
    
        lastTemperature = temperature;
      }
    }
    
  }
  
  
  input.close();
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"ext\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  }
  
  std::cout << "End compute ext temperature" << '\n';
  
  output.close();
}

// [[Rcpp::export]]
void CPPparseAcceleration(std::string data_path, std::string output_path, StringVector mandrill_uuid, std::string timeS, std::string timeE) {
  
  std::vector<std::string> ro;
  
  std::ifstream input( data_path );
  
  for( std::string line; getline( input, line ); )
  {
    
    vector<string> vline = split(line, "_");
    
    int size = vline.size();
    
    bool exact = true;
    
    if(size >= 23) {
      exact = true;
    } else if(size == 1) {
      vline = split(line, ",");
      size = vline.size();
      if(size >= 23) {
        exact = true;
      }
    }
    
    if(size == 1) {
      exact = false;
    }
    
    if(exact) {
      
      //Parse date
      std::string sdate = vline[0];
      std::vector<string> dline = split(line, " ");
      
      if(dline.size() < 5) {
        continue;
      }
      
      std::string month = months[dline[1]];
      std::string realDate = dline[3] + "-" + month + "-" + dline[2] + " " + dline[4];
      
      //Parse uuid
      std::string uuid = vline[3];
      
      bool find = false;
      
      for(int i=0; i < mandrill_uuid.size(); i++){
        if(mandrill_uuid[i] == uuid) {
          find = true;
        }
      }
      
      if(mandrill_uuid.size() == 1) {
        if(mandrill_uuid[0] == "") {
          find = true;
        }
      }
      
      int sizeAcceleration = std::stoi(vline[19]);
      
      if(sizeAcceleration <= 100) {
        find = false;
      }
      
      if(find) {
        //Parse acceleration
        time_t t = dateToint(realDate);
        time_t ts = dateToint(timeS);
        time_t te = dateToint(timeE);
        
        
        struct tm * timeinfo;
        timeinfo = localtime ( &t );
        
        if(timeinfo->tm_hour == 0 && timeinfo->tm_min == 0 && timeinfo->tm_sec == 0) {
          std::cout << intTodate(t) << '\n';
        }
        
        if(t >= ts) {
          if(t <= te) {
            
            time_t t2 = t;
            
            int index_element = 21;
            
            int group = sizeAcceleration / 3;
            
            for(int i=21; i < (group+21); i++){
              
              std::string sx = vline[index_element];
              std::string sy = vline[index_element+1];
              std::string sz = vline[index_element+2];
              index_element += 3;
              
              if(is_number(sx) && is_number(sy) && is_number(sz)) {
                
                int ix = std::stoi(sx);
                int iy = std::stoi(sy);
                int iz = std::stoi(sz);
                
                //if(ix == 0 && iy == 0 && iz == 0) {
                    // nothing to do
                //} else {
                
                  if(t2 >= ts) {
                    if(t2 <= te) {
                      std::string sacceleration = std::to_string(ix) + ";" + std::to_string(iy) + ";" + std::to_string(iz);
                      std::string newdate = intTodate(t2);
                      
                      
                      std::string result = newdate + ";\"" + uuid + "\";" + sacceleration + "\n";
                    
                      
                      //output<<result<< '\n';
                      ro.push_back(result);
                    } else {

                    }
                  }
                //}
                
                t2 += 1;
              } 
            }
          } else {

          }
        }
      }
    }
  }
  
  input.close();  
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"uuid\";\"x\";\"y\";\"z\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  } 
  
  output.close();
}

// [[Rcpp::export]]
void CPPcomputeMouvement(std::string data_path, std::string output_path, StringVector mandrills_uuid, std::string date_start, int compute_time) {
  
  std::cout << "Start compute mouvement" << '\n';
  
  std::map<std::string, MandrillM> mandrills;
  
  time_t timeDateStart = dateToint(date_start);
  
  for(int i=0; i < mandrills_uuid.size(); i++){
    std::string key = Rcpp::as< std::string >(mandrills_uuid(i));
    mandrills[key] = MandrillM();
  }

  std::vector<std::string> ro;
  
  std::ifstream input( data_path );
  
  bool first = true;
  
  bool initDate = true;
  
  for( std::string line; getline( input, line ); )
  {
    
    if(first) {
      first = false;
    } else {
      
      vector<string> vline = split(line, ";");
      
      std::string date = vline[0];
      std::string uuid = vline[1];
      uuid = uuid.substr(1,uuid.size()-2);
      
      double mouvement = std::stod(vline[2]);
      
      time_t intD = dateToint(date);
      
      
      if(initDate) {
        
        while(intD >= (timeDateStart + compute_time)) {
          timeDateStart += compute_time;
        }
        initDate = false;
        
        mandrills[uuid].addMouvement(mouvement);
      }
      else {
        if(intD > timeDateStart + compute_time) {
          timeDateStart += compute_time;
          
          struct tm * timeinfo;
          timeinfo = localtime ( &timeDateStart );
          
          if(timeinfo->tm_hour == 0 && timeinfo->tm_min == 0 && timeinfo->tm_sec == 0) {
            std::cout << intTodate(timeDateStart) << '\n';
          }
          
          std::string dateCompute = intTodate(timeDateStart);
          
          for (auto x : mandrills)
          {
            std::string result = dateCompute + ";\"" + x.first + "\";"+ std::to_string(x.second.mouvement) + ";" + std::to_string(x.second.iteration) + "\n";
            //output<<result;
            ro.push_back(result);
            //cout << result << '\n';
            x.second.rezet();
          }
          
        } else {
          mandrills[uuid].addMouvement(mouvement);
        }
      }
    } 
  }
  
  input.close();
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"uuid\";\"mouvement\";\"iteration_mouvement\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  } 
  
  std::cout << "End compute mouvement" << '\n';
  
  output.close();
}

// [[Rcpp::export]]
void CPPparseMouvement(std::string data_path, std::string output_path, StringVector mandrill_uuid, std::string timeS, std::string timeE) {
 
  std::ifstream input( data_path );
  
  std::vector<std::string> ro;
  
  for( std::string line; getline( input, line ); )
  {
   
    //cout << line << "\n";
   
    vector<string> vline = split(line, "_");
    
    int size = vline.size();
    
    bool exact = false;
    
    if(size >= 23) {
      exact = true;
    } else if(size == 1) {
      vline = split(line, ",");
      size = vline.size();
      if(size >= 23) {
        exact = true;
      }
    }
    
    if(size == 1) {
      exact = false;
    }
    
    if(exact) {
  
      //Parse date
      std::string sdate = vline[0];
      std::vector<string> dline = split(line, " ");
      
      if(dline.size() < 5) {
        continue;
      }
      
      std::string month = months[dline[1]];
      std::string realDate = dline[3] + "-" + month + "-" + dline[2] + " " + dline[4];
      
      //Parse uuid
      std::string uuid = vline[3];
      
      bool find = false;
      
      for(int i=0; i < mandrill_uuid.size(); i++){
        if(mandrill_uuid[i] == uuid) {
          find = true;
        }
      }
      
      if(mandrill_uuid.size() == 1) {
        if(mandrill_uuid[0] == "") {
          find = true;
        }
      }
      
      int sizeMouvement = std::stoi(vline[19]);
      
      if(sizeMouvement <= 100) {
        find = false;
      }
      
      if(find) {
    
        //Parse mouvement
        int mouvement = 0;
  
        time_t t = dateToint(realDate);
        time_t ts = dateToint(timeS);
        time_t te = dateToint(timeE);
        
        struct tm * timeinfo;
        timeinfo = localtime ( &t );
        
        if(timeinfo->tm_hour == 0 && timeinfo->tm_min == 0 && timeinfo->tm_sec == 0) {
          std::cout << intTodate(t) << '\n';
        }
      
        if(t >= ts) {
          if(t <= te) {
            
            time_t t2 = t;
          
            for(int i=22; i < (sizeMouvement+22); i++){
        
              if(is_number(vline[i])) {
                int value = std::stoi(vline[i]);
                mouvement = value;
                
                if(t2 >= ts) {
                  if(t2 <= te) {
                    std::string smouv = std::to_string(mouvement);
                    std::string newdate = intTodate(t2);
                    std::string result = newdate + ";\"" + uuid + "\";" + smouv + "\n";
                    //output<<result<< '\n';
                    ro.push_back(result);
                    

                    //cout << result << "\n";
                  } else {
                  
                  }
                }
                
                t2 += 1;
              } 
            }
          } else {
         
          }
        }
      }
    }
  }
  
  input.close();
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"uuid\";\"mouvement\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  }
  
  output.close();
}

// [[Rcpp::export]]
void CPPcomputeTemperature(std::string data_path, std::string output_path, StringVector mandrills_uuid, std::string date_start, int compute_time) {
  
  std::cout << "Start compute temperature" << '\n';
  
  //std::cout << mandrills_uuid.size() << '\n';
  
  std::map<std::string, MandrillT> mandrills;
  
  time_t timeDateStart = dateToint(date_start);
  
  for(int i=0; i < mandrills_uuid.size(); i++){
    std::string key = Rcpp::as< std::string >(mandrills_uuid(i));
    mandrills[key] = MandrillT();
  }
  
  std::vector<std::string> ro;
  
  //std::cout << data_path << '\n';
  
  std::ifstream input( data_path );
  
  bool first = true;
  
  bool initDate = true;
  
  for( std::string line; getline( input, line ); )
  {
    
    if(first) {
      first = false;
    } else {
      
      vector<string> vline = split(line, ";");
      
      std::string date = vline[0];
      std::string uuid = vline[1];
      uuid = uuid.substr(1,uuid.size()-2);
      
      double temperature = std::stod(vline[2]);
      
      time_t intD = dateToint(date);
      
      
      if(initDate) {
      
        while(intD >= (timeDateStart + compute_time)) {
          timeDateStart += compute_time;
        }
        initDate = false;
        
        mandrills[uuid].addTemperature(temperature);
      }
      else {
        if(intD > timeDateStart + compute_time) {
          timeDateStart += compute_time;
          
          struct tm * timeinfo;
          timeinfo = localtime ( &timeDateStart );
          
          if(timeinfo->tm_hour == 0 && timeinfo->tm_min == 0 && timeinfo->tm_sec == 0) {
            std::cout << intTodate(timeDateStart) << '\n';
          }
          
          std::string dateCompute = intTodate(timeDateStart);
          
          for (auto const& x : mandrills)
          {
           
           MandrillT m =  x.second;
            
           double computeT = m.getComputeT();
          
           if(!isnan(computeT)) {
             if(m.iteration > 0) {
                std::string stemp = to_string(computeT);
                std::string result = dateCompute + ";\"" + x.first + "\";"+ stemp.substr(0, stemp.find(".") + 2) + "\n";
                //output<<result<< '\n';
                ro.push_back(result);
             }
           }
            
           m.rezet();
          }
          
        } else {
          mandrills[uuid].addTemperature(temperature);
        }
      }
    } 
  }
  
  input.close();
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"uuid\";\"temperature\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  } 
  
  std::cout << "End compute temperature" << '\n';
  
  output.close();
}


// [[Rcpp::export]]
void CPPparseTemperature(std::string data_path, std::string output_path, StringVector mandrill_uuid, std::string timeS, std::string timeE) {

  std::vector<std::string> ro;

  std::ifstream input( data_path );
  
  for( std::string line; getline( input, line ); )
  {
    //std::cout << line << '\n';
    
    vector<string> vline = split(line, "_");
    
    int size = vline.size();
    
    
    bool exact = false;
      
    if(size >= 24 && size <= 26) {
      if(vline[16] != "undefined" && vline[15] != "undefined") {
        exact = true;
      }
    } else if(size == 1) {
      vline = split(line, ",");
      size = vline.size();
      if(size >= 124) {
        if(vline[16] != "undefined" && vline[15] != "undefined") {
          exact = true;
        }
        
      }
    }
    
    if(size == 1) {
        exact = false;
    }
      
    if(exact) {
      
      //std::cout << "ok" << '\n';
      
      //Parse date
      std::string sdate = vline[0];
      std::vector<string> dline = split(line, " ");
      
      if(dline.size() < 5) {
        continue;
      }
      
      std::string month = months[dline[1]];
      std::string realDate = dline[3] + "-" + month + "-" + dline[2] + " " + dline[4];
      
      //Parse uuid
      std::string uuid = vline[3];
      
      //std::cout << uuid << '\n';
      
      bool find = false;
      
      for(int i=0; i < mandrill_uuid.size(); i++){
        if(mandrill_uuid[i] == uuid) {
          find = true;
        }
      }
      
      if(mandrill_uuid.size() == 1) {
        if(mandrill_uuid[0] == "") {
          find = true;
        }
      }
      
      if(find) {
        
        //std::cout << "find" << '\n';

        //Parse temperature
        int df1 = std::stoi(vline[22]);
        int df2 = std::stoi(vline[23]);
        
        std::string hdf1 = dec2hex(df1);
        std::string hdf2 = dec2hex(df2);
        
        std::string C0 = hdf2;
        
        if(df2 < 16) {
          C0 = "0"+C0;
        }
        
        std::string HC = hdf1 + C0;
        
        unsigned long dh1 = std::stoul(HC, nullptr, 16);
        unsigned long dh2 = dh1;
        
        std:string dh = dec2hex(dh2);
        
        int sizeDH = dh.size();
        
        if(sizeDH < 4) {
          dh = std::string(4 - sizeDH, '0') + dh;
        }
      
        std::string vh1 = dh.substr(0,2); 
        std::string vh2 = dh.substr(2,3); 
        
        std::string hb1 = decToBinary(std::stoi(vh1, nullptr, 16));
        std::string hb2 = decToBinary(std::stoi(vh2, nullptr, 16));
        
        unsigned long sizeHB1 = hb1.size();
        unsigned long sizeHB2 = hb2.size();
        
        if(sizeHB1 < 8) {
          hb1 = std::string(8 - sizeHB1, '0') + hb1;
        }
        
        if(sizeHB2 < 8) {
          hb2 = std::string(8 - sizeHB2, '0') + hb2;
        }
        
        std::string tmpbv = hb1 + hb2;
        std::string bv = tmpbv.substr(4, 15);
        std::string bv2 = bitwNot(bv);
        
        int sizeBV = bv2.size();
        
        std::string bv3 = bv2.substr(sizeBV-11-1, sizeBV-1);
  
        double temperature = 0.0;
        
        if(bv3[0] == '0') {
          temperature = 0.0;
        } else {
          temperature = (double)dh1 / 10.0;
        }
        
        if(temperature > 0) {
          std::string stemp = std::to_string(temperature);
          std::string result = realDate + ";\"" + uuid + "\";" + stemp.substr(0, stemp.find(".") + 2) + "\n";
          
          //cout<<result<<endl;
          
          time_t t = dateToint(realDate);
          time_t ts = dateToint(timeS);
          time_t te = dateToint(timeE);
          
          struct tm * timeinfo;
          timeinfo = localtime ( &t );
          
          if(timeinfo->tm_hour == 0 && timeinfo->tm_min == 0 && timeinfo->tm_sec == 0) {
            std::cout << intTodate(t) << '\n';
          }
    
          if(t >= ts) {
            
            if(t <= te) {
              //output<<result<< '\n';
              ro.push_back(result);
            
            } else {
           
            }
          }
        } 
      }
    }
  }
  
  
  input.close();
  
  std::ofstream output(output_path);
  
  output<<"\"date\";\"uuid\";\"temperature\""<< '\n';
  
  for(auto li : ro) {
    output << li;
  }
  
  output.close();
}

