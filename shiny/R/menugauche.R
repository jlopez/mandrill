MenuGauche = sidebarMenu(id = "sidebarmenu",
                         menuItem("Home", tabName = "Home",  icon = icon("home", lib="font-awesome")),
                         
                         selectInput("pathMandrill", "Week:",
                                     PATHWEEK),
                        
                         
                         selectInput("uuidMandrill", "Mandrill:", multiple = FALSE, selected = "32-38-31-39-58-36-8e-0c",
                                     UUIDMANDRILL),
                         
                         
                         
                         checkboxGroupInput("checkGroupParam", label = "Diagramme :", 
                                            choices = list("Temperature" = 1, "Acceleration X" = 2, "Acceleration Y" = 3, "Acceleration Z" = 4),
                                            selected = c(1)),
                         
                         textInput("penaltyCP", "Temperature penalty change point :", "6/10 * log(n)"),
                         
                         sliderInput("temperatureCP", "Temperature change point (Celcius) :",
                                     min = 0, max = 3, step = 0.1, value = 0.5),
                         
                         textInput("penaltyCPM", "Mouvement penalty change point (algo):", "30*log(n)"),
                         
                         sliderInput("mouvementCP", "Mouvement maximum mean :",
                                     min = 0, max = 1, step = 0.01, value = 0.3),
                         
                         sliderInput("sizeCPM", "Minimum mouvement range size change point (minute):",
                                     min = 0, max = 120, step = 1, value = 30),
                         
                         br(), 
                        
                         
                         actionButton("runButton", "Run"),

                         tags$br(), tags$br(), tags$br(),
                         
                         menuItem("Team", icon = icon("book", lib="font-awesome"),
                                  menuItem("Jimmy Lopez",  href = "http://www.isem.univ-montp2.fr/recherche/les-plate-formes/bioinformatique-labex/personnel/", newtab = TRUE,   icon = shiny::icon("male"), selected = NULL  )
                         )
                         
)
